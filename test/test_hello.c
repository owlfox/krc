#include "unity.h"
#include "hello.h"
#include <stdio.h>

const size_t SIZE = 255;
static char buffer[SIZE];
void setUp(void)
{
    // freopen("/dev/null", "a", stdout);
    setbuf(stdout, buffer);
    real_main();
    // freopen("/dev/tty", "a", stdout);
    
    
    

}

void tearDown(void)
{
    
}

void test_hello(void)
{
    

    TEST_ASSERT_EQUAL_STRING("hello", buffer);
    


}

// resources of how to tes stdio
// https://codereview.stackexchange.com/questions/212155/simple-unit-test-in-c-of-input-using-freopenhttps://codereview.stackexchange.com/questions/212155/simple-unit-test-in-c-of-input-using-freopen
// https://stackoverflow.com/questions/14428406/redirect-output-from-stdout-to-a-string
// it looks like it's not this portable to test this way..
// https://stackoverflow.com/questions/1908687/how-to-redirect-the-output-back-to-the-screen-after-freopenout-txt-a-stdo